/**
 * Behavior of the main menu.
 */
(function ()
{
    'use strict'
    
    var elements = 
    {
        main_menu: $('#main-menu'),
        main_menu_bar: $('#main-menu-bar'),
        menu_items: $('#main-menu-bar .main-menu-item'),
        expanded_background: $('#main-menu-expanded-background'),
        small_menu_trigger: $('#small-menu-trigger'),
        main_header: $('#main-header')
    }
    
    function transition(state, animate)
    {
        Object.keys(state).forEach(function (element_name)
        {
            if (animate)
            {
                elements[element_name].transit(state[element_name])
            }
            else
            {
                elements[element_name].css(state[element_name])
            }
        })
    }
    
    var LargeScreenMenuBehavior = function ()
    {
        this.expand_delay = 800
        this.expand_timeout = null
        this.is_expanded = null
        
        this.states = 
        {
            expanded:
            {
                expanded_background:
                {
                    width: '15.625rem'
                },
                
                main_menu:
                {
                    'background-color': '#fff'
                }
            },
            
            contracted:
            {
                expanded_background:
                {
                    width: 0
                },
                
                main_menu:
                {
                    bottom: 0,
                    'background-color': 'transparent'
                }
            }
        }
        
        this.onmouseenter = function (e)
        {
            if (!this.expand_timeout)
            {
                this.expand_timeout = setTimeout(this.expand, this.expand_delay)
            }
        }.bind(this)
        
        
        this.onmouseleave = function (e)
        {
            if (this.expand_timeout)
            {
                clearTimeout(this.expand_timeout)
                this.expand_timeout = null
            }
            
            if (this.is_expanded)
            {
                this.contract()
            }
        }.bind(this)
        
        
        this.ontouchstart = function (e)
        {
            if (!this.is_expanded)
            {
                this.expand()
                e.stopPropagation()
                e.preventDefault()
                $(document).on('touchstart', this.document_ontouchstart)
            }
        }.bind(this)
        
        
        this.document_ontouchstart = function (e)
        {
            this.contract(true)
            $(document).off('touchstart', this.document_ontouchstart)
        }.bind(this)
        
        
        this.expand = function ()
        {
            if (this.is_expanded)
            {
                return
            }
            
            this.is_expanded = true
            elements.menu_items.addClass('active')
            transition(this.states.expanded, true)
        }.bind(this)
        
        
        this.contract = function (animated, force)
        {
            if (!this.is_expanded && !force)
            {
                return
            }
            
            this.is_expanded = false
            elements.menu_items.removeClass('active')
            transition(this.states.contracted, animated)
        }.bind(this)
        
        this.contract(false, true)
        
        elements.main_menu.on('touchstart', this.ontouchstart)
        elements.main_menu.on('mouseenter', this.onmouseenter)
        elements.main_menu.on('mouseleave', this.onmouseleave)
        
        this.onunload = function ()
        {
            transition(this.states.contracted)
            elements.main_menu.off('touchstart', this.ontouchstart)
            $(document).off('touchstart', this.document_ontouchstart)
            elements.main_menu.off('mouseenter', this.onmouseenter)
            elements.main_menu.off('mouseleave', this.onmouseleave)
        }.bind(this)
    }
    
    var SmallScreenMenuBehavior = function ()
    {
        this.is_expanded = false
        
        this.states = 
        {
            expanded:
            {
                main_header:
                {
                    top: '100%',
                    transform: 'translate(0, -100%)'
                },
                
                main_menu:
                {
                    bottom: 0
                }
            },
            
            contracted:
            {
                main_header:
                {
                    top: 0,
                    transform: 'translate(0, 0)'
                },
                
                main_menu:
                {
                    bottom: '100%',
                    'background-color': 'rgba(255,255,255,0.9)'
                }
            }
        }
        transition(this.states.contracted)
        
        this.onclick = function (e)
        {
            e.preventDefault()
            
            if (this.is_expanded)
            {
                this.is_expanded = false
                elements.small_menu_trigger.text('Menu')
                transition(this.states.contracted, true)
            }
            else
            {
                this.is_expanded = true
                elements.small_menu_trigger.text('Close')
                transition(this.states.expanded, true)
            }
        }.bind(this)
        
        
        elements.small_menu_trigger.on('touchstart click', this.onclick)
        
        this.onunload = function ()
        {
            transition(this.states.contracted)
            elements.small_menu_trigger.off('touchstart click', this.onclick)
        }.bind(this)
    }
    
    var current_breakpoint = null,
        current_behavior = null
    
    function update_handler_from_breakpoint ()
    {
        var breakpoint = $.breakpoint == 'large' || $.breakpoint == 'medium' ? 'large' : 'small'
        
        if (breakpoint != current_breakpoint)
        {
            if (current_behavior && current_behavior.onunload)
            {
                current_behavior.onunload()
            }
            
            current_breakpoint = breakpoint
            current_behavior = breakpoint == 'large' ? new LargeScreenMenuBehavior() : new SmallScreenMenuBehavior()
        }
    }
    
    update_handler_from_breakpoint()
    $(window).resize(update_handler_from_breakpoint)
})();