/*
 * Show/hide behavior for collapsable blocks.
 * 
 * Example:
 * 
 * <div class="collapsable collapsed" id="some-box">You can't see me on page load</div>
 * <div class="collapse-toggle" for="some-box">
 *    <div class="collapsed-content">Click me to show the box</div>
 *    <div class="expanded-content">Click me to hide the box</div>
 * </div>
 */
(function ()
{
    "use strict"
    
    $('.collapse-toggle').each(function ()
    {
        var $toggle = $(this),
            $box = $('#' + $toggle.attr('for'))
        
        $toggle.on('touchstart click', function (e)
        {
            e.preventDefault()
            
            if ($toggle.is('.collapsed'))
            {
                $toggle.removeClass('collapsed')
                $box.removeClass('collapsed')
            }
            else
            {
                $toggle.addClass('collapsed')
                $box.addClass('collapsed')
            }
        })
    })
})();