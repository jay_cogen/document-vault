/*
 * Convert semantic markup into simple graphs.
 */
(function ()
{
    "use strict"
    
    /* Horizontal stacked bar w/ legend */
    $('.graph.stacked-bar').each(function ()
    {
        var $elm = $(this),
            $bar_group = $('<div class="bar-group"></div>').appendTo($elm),
            $legend = $('<div class="legend"></div>').appendTo($elm)
        
        $elm.find('.bar').appendTo($bar_group).each(function ()
        {
            var $bar = $(this)
            $bar.css({
                width: ($bar.data('amount') * 100) + '%',
                backgroundColor: $bar.data('color')
            })
            $legend.append(
                $('<div class="legend-key inline-middle hspace-1 vspace-2 color-light-cold-gray type-caps type-small-light"></div>')
                        .append(
                            $('<div class="legend-key-color"></div>').css('background-color', $bar.data('color'))
                        )
                        .append('<span>'+$bar.data('label')+'</span>')
            )
        })
        
        $elm.find('.bar-marker').each(function ()
        {
            var $marker = $(this)
            $marker.css('left', ($marker.data('position') * 100) + '%')
        })
    })
})();