(function(){

    //Let's make the magic happen and kick things off
    var app = angular.module('docVault', ['flow','jsonService']);

    var theVault;

    app.controller('vaultController', function($scope, JsonService){

        //GET THE DATA

        $scope.folderArea = new JsonService();
        $scope.theVault = JsonService.query();


        //CRUD FOLDERS

        $scope.saveFolder = function(folder){
            if(folder.folder){
                if(folder.id){
                    return folder.$update();
                } else {
                    $scope.folderArea.$save().then(function(response){
                        $scope.theVault.push(response)
                    });
                    $scope.folderArea = new JsonService();
                    $('.addNewFolderArea').slideToggle();
                }
            }
        }


        $scope.deleteFolder = function(folder){
            JsonService.delete(folder, function(){
                for(var i=0; i<$scope.theVault.length; i++){
                    if($scope.theVault[i].id == folder.id){
                        $scope.theVault.splice(i, 1);
                        break;
                    }
                }
            });
        }



        //CRUD FILES






    });






    //DIRECTIVES

    //Attributes

    app.directive("toggleNewFolder", function(){
        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {
                $(elem).click(function() {
                    $(elem).parent().siblings('.addNewFolderArea').slideToggle();
                })
            }
        }
    });

    app.directive("toggleFiles", function(){
        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {
                $(elem).click(function() {
                    $(elem).toggleClass('showing');
                    $(elem).parent().siblings('.files-area').slideToggle();
                })
            }
        }
    });

    app.directive("editFolderName", function(){
        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {
                $(elem).click(function() {
                    console.log(scope.editMode);
                    scope.editMode = true;
                    console.log(scope.editMode);
                })
            }
        }
    })




    //Elements

    app.directive("topArea", function(){
        return {
            restrict: 'E',
            templateUrl: "partials/top-area.html"
        };
    });

    app.directive("sidebarCode", function(){
        return {
            restrict: 'E',
            templateUrl: "partials/sidebar.html"
        };
    });

})();