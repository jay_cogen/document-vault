/* Behavior of in-page tab navigation */
(function ()
{
    "use strict"
    
    $('.tabs').each(function ()
    {
        var $tabs = $(this),
            $all_labels = $tabs.find('.tab-label'),
            $all_contents = $tabs.find('.tab-content')
        
        $all_labels.on('touchstart click', function ()
        {
            var $label = $(this)
            
            if ($label.is('.selected'))
            {
                return
            }
            
            $all_labels.removeClass('selected')
            $label.addClass('selected')
            $all_contents.removeClass('selected')
            $tabs.find('#' + $label.attr('for')).addClass('selected')
        })
    })
})();