angular.module('jsonService', ['ngResource'])
    .factory('JsonService', function($resource) {
        return $resource('http://0.0.0.0:3000/api/folders/:id', { id: '@id' }, {
            update: { method: 'PUT'}
        });

});