/*
 * Show/hide behavior for the "My Account" menu 
 */
(function ()
{
    "use strict"
    
    var $document = $(document),
        $account = $('#my-account'),
        $show_button = $('.show-account'),
        $hide_button = $('.hide-account'),
        show = function ()
        {
            $account.transit({ x: 0 })
            setTimeout(function ()
            {
                $document.on('touchstart click', hide)
            })
        },
        hide = function ()
        {
            $account.transit({ x: '100%' })
            setTimeout(function ()
            {
                $document.off('touchstart click', hide)
            })
        }
        
    $show_button.on('touchstart click', function (e)
    {
        e.preventDefault()
        show()
    })
    
    $hide_button.on('touchstart click', function (e)
    {
        e.preventDefault()
        hide()
    })
    
    $account.on('touchstart click', function (e)
    {
        e.stopPropagation()
    })
})();