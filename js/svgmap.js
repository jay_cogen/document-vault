/**
 * Replace sprite divs with inline SVG from the sprite map. This is the only
 * reliable method of sprite mapping SVGs while maintaining the ability to style
 * them with CSS.
 *
 * Example:
 *
 *  <div class="sprite calendar"></div>
 *
 * The "sprite" class triggers processing as a sprite. The "calendar" class is the 
 * id of a <view> inside ./images/map.svg. This element will be replace with an <svg> 
 * element whose children are a clone of the matching <view>.
 */

(function ()
{
    "use strict"
    
    var frame = document.createElement('iframe')
    frame.style.visibility = 'hidden'
    frame.src = 'images/map.svg'
    frame.onload = function ()
    {
        var views = this.contentDocument.getElementsByTagName('view')
        Array.prototype.slice.apply(views).forEach(function (view)
        {
            var sprite_markers = document.querySelectorAll('.sprite.' + view.id)
            
            if (sprite_markers.length > 0)
            {
                var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
                svg.setAttribute('viewBox', view.getAttribute('viewBox'))
                
                Array.prototype.slice.apply(view.childNodes).forEach(svg.appendChild.bind(svg))
                Array.prototype.slice.apply(sprite_markers).forEach(function (sprite_marker)
                {
                    var sprite = svg.cloneNode(true)
                    sprite.setAttribute('class', sprite_marker.className.replace('sprite', ''))
                    sprite_marker.parentNode.replaceChild(sprite, sprite_marker)
                })
            }
        })
    }
    document.querySelector('body').appendChild(frame)
})()