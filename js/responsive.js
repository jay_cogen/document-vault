/**
 * Detects and reports the current breakpoint.
 */
(function ()
{
    "use strict"
    
    var $detector = $('<div class="breakpoint"></div>').appendTo($('body')),
        responsive_elements = 
        {
            small: $('[data-class-small]'),
            medium: $('[data-class-medium]'),
            large: $('[data-class-large]')
        }
    
    function detect ()
    {
        var new_breakpoint
        
        switch ($detector.css('font-weight'))
        {
            case '600':
                new_breakpoint = 'large'
                break
            case '400':
                new_breakpoint = 'medium'
                break
            case '200':
                new_breakpoint = 'small'
                break
            default:
                new_breakpoint = 'large'
        }
        
        if (new_breakpoint != $.breakpoint)
        {
            if ($.breakpoint)
            {
                responsive_elements[$.breakpoint].each(function ()
                {
                    var $elm = $(this)
                    $elm.removeClass($elm.data('class-'+$.breakpoint))
                })
            }
            
            $.breakpoint = new_breakpoint
            
            responsive_elements[$.breakpoint].each(function ()
                {
                    var $elm = $(this)
                    $elm.addClass($elm.data('class-'+$.breakpoint))
                })
        }
    }
    
    detect()
    
    $(window).resize(detect)
})();